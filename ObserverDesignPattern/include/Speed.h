#pragma once

#include "Observer.h"

class Speed : public Observer{
private:
    float   first_value,
            second_value;
    bool    hasFirst_value = false,
            hasSecond_value = false;
public:
    float get()const;
    void notify(float);
};