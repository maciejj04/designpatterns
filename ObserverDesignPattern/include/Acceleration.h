#pragma once

#include "Observer.h"

class Acceleration : public Observer{
    float   first_value,
            second_value,
            third_value;
    bool    hasfirst_value = false,
            hasSecond_value = false,
            hasThird_value = false;
public:
    float get()const;
    void notify(float Newvalue);
};