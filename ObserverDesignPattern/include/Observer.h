#pragma once

class Observer {
public:
    virtual void notify(float value){}
};