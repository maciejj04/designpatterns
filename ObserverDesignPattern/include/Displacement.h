#pragma once

#include "Observable.h"

class Displacement : public Observable{
private:
    float value;
public:
    void set(float val);
};

