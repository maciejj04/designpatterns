#pragma once

#include "Observer.h"
#include <set>

class Observable{
    std::set<Observer*> Observers;
protected:
    void notifyObservers(float)const;

public:
    void AddObserver(Observer& obj);
    void DeleteObserver(Observer& obj);
};
