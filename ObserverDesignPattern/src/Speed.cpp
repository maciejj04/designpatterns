#include "../include/Speed.h"
#include <stdio.h>

void Speed::notify(float value){
    if(hasSecond_value && hasFirst_value){
        first_value = second_value;
        second_value = value;
    }
    else if(!hasFirst_value && !hasSecond_value){
        first_value = value;
        hasFirst_value = true;
    }
    else if(hasFirst_value && !hasSecond_value){
        second_value = value;
        hasSecond_value = true;
    }

}
float Speed::get() const {
    if(hasFirst_value && hasSecond_value){
        return second_value - first_value;
    }
    else{
        printf("ERROR: To get() the speed you have to have two speed values!\n");
        return  -1.0f;
    }

}