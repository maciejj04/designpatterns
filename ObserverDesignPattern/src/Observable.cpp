#include "../include/Observable.h"

void Observable::notifyObservers(float value)const{
    for (Observer* observer: Observers)
        observer->notify(value);
}
void Observable::AddObserver(Observer& observer){
    Observers.insert(&observer);
}
void Observable::DeleteObserver(Observer& observer){
    Observers.erase(&observer);
}