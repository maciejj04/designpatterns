#include "../include/Acceleration.h"
#include <stdio.h>
void Acceleration::notify(float Newvalue){
    if(hasfirst_value&&hasSecond_value&&hasThird_value){
        first_value = second_value;
        second_value = third_value;
        third_value = Newvalue;
    }
    else if(hasfirst_value&&hasSecond_value){
        hasThird_value = true;
        third_value = Newvalue;
    }
    else if(hasfirst_value){
        hasSecond_value = true;
        second_value = Newvalue;
    }
    else{
        hasfirst_value = true;
        first_value = Newvalue;
    }
}
float Acceleration::get()const{
    if(hasfirst_value&&hasSecond_value&&hasThird_value){
        return third_value - 2*second_value + first_value;
    }else{
        printf("ERROR: To get() the acceleration you have to have three speed values!\n");
        return -1.0f;
    }

}