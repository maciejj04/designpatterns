#include "../include/Displacement.h"

void Displacement::set(float val){
    value = val;
    notifyObservers(val);
}