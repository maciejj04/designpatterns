#include <iostream>
#include "../include/Displacement.h"
#include "../include/Speed.h"
#include "../include/Acceleration.h"

int main() {
    Displacement dsp;
    Speed spd;
    Acceleration acclr;

    dsp.AddObserver(spd);
    dsp.AddObserver(acclr);

    dsp.set(10);
    dsp.set(15);

    std::cout << "Speed = " << spd.get() << std::endl;

    dsp.set(17);

    std::cout << "Acceleration = " << acclr.get() << std::endl
              << "Speed = " << spd.get() << std::endl;

    return 0;
}