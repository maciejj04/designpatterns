#include <iostream>
#include "include/GuiFactory.h"

int main() {

    auto factory = GuiFactory::create("InteliJ");
    auto button = factory->createButton();
    auto list = factory->createList();

    button->click();
    list->size();

    return 0;


}