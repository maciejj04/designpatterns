#include <memory>
#include "../include/Button.h"
#include "../include/List.h"
#include "../include/GreyscaleGuiFactory.h"
#include "../include/DraculaGuiFactory.h"
#include "../include/InteliJGuiFactory.h"

std::unique_ptr<Button> InteliJGuiFactory::createButton()
{   return new InteliJButton;  }
std::unique_ptr<List> InteliJGuiFactory::createList()
{   return new InteliJList;    }

