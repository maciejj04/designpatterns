#include "../include/GuiFactory.h"
#include "../include/InteliJGuiFactory.h"
#include "../include/DraculaGuiFactory.h"
#include "../include/GreyscaleGuiFactory.h"


std::unique_ptr<GuiFactory> GuiFactory::create(std::string type){

    if( type == "DraculaGui" )
        return std::make_unique<DraculaGuiFactory>();

    if( type == "InteliJGui" )
        return std::make_unique<InteliJGuiFactory>();

    if( type == "GreyscaleGui" )
        return std::make_unique<GreyscaleGuiFactory>();
}
