#include <memory>
#include "../include/Button.h"
#include "../include/List.h"
#include "../include/GreyscaleGuiFactory.h"
#include "../include/GreyscaleList.h"
#include "../include/GreyscaleButton.h"

std::unique_ptr<Button> GreyscaleGuiFactory::createButton()
    {   return std::make_unique<GreyscaleButton>();  }
std::unique_ptr<List> GreyscaleGuiFactory::createList()
    {   return new GreyscaleList;    }
