#include <memory>
#include "../include/Button.h"
#include "../include/List.h"
#include "../include/GreyscaleGuiFactory.h"
#include "../include/DraculaGuiFactory.h"

std::unique_ptr<Button> DraculaGuiFactory::createButton()
{   return new DraculaButton;  }
std::unique_ptr<List> DraculaGuiFactory::createList()
{   return new DraculaList;    }
