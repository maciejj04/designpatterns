#pragma once

#include <bits/unique_ptr.h>
#include "Button.h"
#include "List.h"

class InteliJGuiFactory{
public:
    std::unique_ptr<Button> createButton();
    std::unique_ptr<List> createList();

};