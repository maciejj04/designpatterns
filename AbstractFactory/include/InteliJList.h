#pragma once

#include "List.h"

class InteliJList : public List{
    int size() const override ;
};
