#pragma once

#include <memory>
#include "Button.h"
#include "List.h"

class GuiFactory {

public:
    static std::unique_ptr<GuiFactory> create(std::string type);

    virtual std::unique_ptr<Button> createButton();
    virtual std::unique_ptr<List> createList();

};
