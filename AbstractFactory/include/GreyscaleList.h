#pragma once

#include "List.h"

class GreyscaleList : public List{
    int size() const ;
};
